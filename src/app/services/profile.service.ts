import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private headerDict = {
    'Content-Type': 'application/json'
  };
  constructor(private httpClient: HttpClient) { }

  public async getProfile(): Promise<any> {
    return await this.httpClient.get('http://127.0.0.1:3001/api/user', {headers: this.headerDict}).toPromise();
  }
}
