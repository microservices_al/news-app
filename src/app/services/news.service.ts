import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  private headerDict = {
    'Content-Type': 'application/json'
  };

  constructor(private httpClient: HttpClient) { }

  public async getNews(): Promise<any> {
    return await this.httpClient.get('http://127.0.0.1:3000/api/news', {headers: this.headerDict}).toPromise();
  }
}
