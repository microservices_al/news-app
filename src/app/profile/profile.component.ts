import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user = {
    dateOfBirth: '',
    userName: '',
    age: ''
  };

  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {
    this.getProfile();
  }

  refresh(): void {
    console.log('REFRESH PROFILE');
    this.getProfile();
  }

  async getProfile() {
    this.setTextsLoading();
    try {
      const profile = await this.profileService.getProfile();
      this.user = profile;
    } catch (err) {
      this.user.userName = 'Could not retrieve username';
      this.user.age = 'Could not retrieve age';
      this.user.dateOfBirth = 'Could not retrieve date of birth';
    }
  }

  setTextsLoading() {
    this.user.dateOfBirth = 'Loading date of birth';
    this.user.userName = 'Loading user name';
    this.user.age = 'Loading age';
  }
}
