import { Component, OnInit } from '@angular/core';
import { NewsService } from '../services/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  headline = '';
  text = '';

  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
    this.getNews();
  }

  refresh(): void {
    console.log('REFRESH NEWS');
    this.getNews();
  }

  async getNews() {
    this.setTextsLoading();
    try {
      const news = await this.newsService.getNews();
      this.headline = news.headline;
      this.text = news.text;
    } catch (err) {
      this.headline = 'Could not retrieve headline';
      this.text = 'Could not retrieve text';
    }
  }

  setTextsLoading() {
    this.headline = 'Loading news';
    this.text = 'Loading news';
  }
}
